package com.xyz.hr.emplyeemgmt.dao;

import com.xyz.hr.emplyeemgmt.model.AccountInfo;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class AccountDao {

    public AccountInfo retrieveAccountInfo(String accNum){
        AccountInfo accountInfo1 = new AccountInfo();
        accountInfo1.setAccountNum("1001");
        accountInfo1.setAccountType("Saving");
        accountInfo1.setRoutingNum("10011001");
        Date date1 = new Date();
        accountInfo1.setAccountOpeningDate(date1);

        AccountInfo accountInfo2 = new AccountInfo();
        accountInfo2.setAccountNum("1002");
        accountInfo2.setAccountType("Checking");
        accountInfo2.setRoutingNum("20011002");
        Date date2 = new Date();
        accountInfo1.setAccountOpeningDate(date2);
        if(accNum.equals("1001"))
            return accountInfo1;
        else
            return accountInfo2;
    }
}
