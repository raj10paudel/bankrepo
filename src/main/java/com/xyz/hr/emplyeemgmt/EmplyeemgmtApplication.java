package com.xyz.hr.emplyeemgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmplyeemgmtApplication {

	public static void main(String[] args) {
		//comment
		SpringApplication.run(EmplyeemgmtApplication.class, args);
	}

}
