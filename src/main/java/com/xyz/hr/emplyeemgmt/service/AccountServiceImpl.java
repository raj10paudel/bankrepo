package com.xyz.hr.emplyeemgmt.service;

import com.xyz.hr.emplyeemgmt.dao.AccountDao;
import com.xyz.hr.emplyeemgmt.model.AccountInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AccountServiceImpl implements AccountService{
    @Autowired
    AccountDao accountDao;

    @Override
    public AccountInfo getAccountInfo(String accountNum) {
        return accountDao.retrieveAccountInfo(accountNum);
    }
}
