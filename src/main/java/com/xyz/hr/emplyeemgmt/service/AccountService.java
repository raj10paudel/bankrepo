package com.xyz.hr.emplyeemgmt.service;

import com.xyz.hr.emplyeemgmt.model.AccountInfo;

public interface AccountService {
    AccountInfo getAccountInfo(String accountNum);
}
