package com.xyz.hr.emplyeemgmt.controller;


import com.xyz.hr.emplyeemgmt.model.AccountInfo;
import com.xyz.hr.emplyeemgmt.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api")
public class BankAccountController {
    @Autowired
    AccountService accountService;

    @GetMapping("/account/{account_num}")
    public AccountInfo getAccountDetails(@PathVariable String account_num){
        return accountService.getAccountInfo(account_num);
    }

    @PostMapping("/account")
    public String addAccountDetails(@RequestBody AccountInfo accountInfo){
        String acctNumCreated = "1005";
        return acctNumCreated;
    }

}
